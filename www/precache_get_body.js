	function precache_getHTML()
	{
		//add the load transition overlay
		var load_div = document.createElement('div');
		load_div.setAttribute('id',"load_overlay");

		document.body.appendChild(load_div);

		//document.getElementById("load_overlay").style.display="block";
		var precache_reference = document.body.getAttribute('precache-reference');
		if(precache_reference){
			if (typeof(Storage) !== "undefined") {
				var content = window.localStorage.getItem(precache_reference);
				if(content === null){
					content = window.sessionStorage.getItem(precache_reference);
				}
				document.body.innerHTML += content;
				//execute all js from the new body
				nodeScriptReplace(document.getElementsByTagName("body")[0]);
				//call onload event
				document.getElementById("load_overlay").addEventListener('animationend',remove_overlay);
				document.getElementById("load_overlay").addEventListener('webkitAnimationEnd',remove_overlay);
				document.getElementById("load_overlay").addEventListener('MSAnimationEnd',remove_overlay);
				document.getElementById("load_overlay").addEventListener('transitionend ',remove_overlay);
				document.getElementById("load_overlay").addEventListener('webkitTransitionEnd',remove_overlay);
				document.getElementById("load_overlay").addEventListener('MSTransitionEnd',remove_overlay);
				
				//setTimeout('triggerEvent(window,"load",null);',300);
				//setTimeout('document.getElementById("load_overlay").style.display="none"',1500);
			} else {
				// Sorry! No Web Storage support..
			}				
			
		}
	
	}
	function remove_overlay(){
		document.getElementById("load_overlay").style.display="none";
	}
	function triggerEvent(el, eventName, options) {
	  var event;
	  if (window.CustomEvent) {
		event = new CustomEvent(eventName, options);
	  } else {
		event = document.createEvent('CustomEvent');
		event.initCustomEvent(eventName, true, true, options);
	  }
	  el.dispatchEvent(event);
	}
		
		
	function nodeScriptReplace(node) {
			if ( nodeScriptIs(node) === true ) {
					if((node.getAttribute('src')===null) || (node.getAttribute('src').indexOf("precache_get_body")<0) || (node.getAttribute('src').indexOf("precaching")<0)){
						node.parentNode.replaceChild( nodeScriptClone(node) , node );
					}
				
			}
			else {
					var i        = 0;
					var children = node.childNodes;
					while ( i < children.length ) {
							nodeScriptReplace( children[i++] );
					}
			}

			return node;
	}
	function nodeScriptIs(node) {
			return node.tagName === 'SCRIPT';
	}
	function nodeScriptClone(node){
			var script  = document.createElement("script");
			script.text = node.innerHTML;
			for( var i = node.attributes.length-1; i >= 0; i-- ) {
					script.setAttribute( node.attributes[i].name, node.attributes[i].value );
			}
			//console.log(node.getAttribute("src"));
			return script;
	}		


	
	precache_getHTML();