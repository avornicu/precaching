
var precache={
	
	limit_reached:false,
	precache_start:function(event)
	{
		setTimeout("precache.do_precache()",100);
	},
	
	//main function - executed when the page has finished loading
	do_precache: function()
	{
		//clean old localStorage entries before caching anything, so we try our best not to exceed the storage space
		this.cleanOldEntries(300);//clean older than 5 min
		
		//add the unload transition overlay
		var unload_div = document.createElement('div');
		unload_div.setAttribute('id',"unload_overlay");
		document.body.appendChild(unload_div);
		
		var precache_node = document.getElementsByTagName("precache");
		
		//create an invisible div at the end of the page where we can load images from the next pages
		var precache_div = document.createElement('div');
		precache_div.setAttribute('id',"precache_html_container");
		precache_div.style.display="none";
		document.body.appendChild(precache_div);
		if(precache_node.length > 0){
			var precache_elements = precache_node[0].getElementsByTagName("element");
			//go through the list of precache elements
			for(var i_elem in precache_elements){
				if(isNaN(i_elem) && (i_elem.search("ms__id")<0)) continue;
				var current_element = precache_elements[i_elem];
				var element_type = current_element.getAttribute("type");
				var element_src = current_element.getAttribute("src");
				var element_name = current_element.getAttribute("name");
				switch(element_type){
					case "image"://load image in an img tag in the invisible div
						var new_image = document.createElement('img');
						new_image.setAttribute('src',element_src);
						precache_div.appendChild(new_image);
						;
					break;
					case "js"://js and css files are loaded as text files in an iframe in the invisible div
					case "css":
					case "pdf":
					case "doc":
					case "download":
						var js_css_element = document.createElement('iframe');
						js_css_element.setAttribute('src',element_src);
						precache_div.appendChild(js_css_element);
					break;
					case "html"://HTML is stored in localstorage
						precache.addHTML(element_src,element_name,false);
					break;
					case "ajax"://AJAX is stored in localstorage
						precache.addHTML(element_src,element_name,true);
					break;
						
				}
				
			}
		}
		this.parse_links_in_page();
		
	},

	//load a page via ajax and store its body content in localstorage under key given by name
	addHTML: function(url,name,is_ajax)
	{
		if(precache.limit_reached){
			console.log('not adding');
			return false;
		}
		if(!precache.existsInCookie(name)){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(precache.limit_reached){
					return false;
				}
				if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
					if (xmlhttp.status == 200) {
						var html_to_store;
						if(is_ajax){
							html_to_store = xmlhttp.responseText;
						}else{
							//get only body from page
							var container = document.implementation.createHTMLDocument('precache_html_'+name).documentElement;
							container.innerHTML = xmlhttp.responseText;
							var html_body = container.querySelector('body');	
							html_to_store = html_body.innerHTML;
							
							//precache images from the page
							var _imgs = html_body.getElementsByTagName('img');
							for (var i_img in _imgs) {
								if(typeof _imgs[i_img] == 'object'){
									var imgsrc = _imgs[i_img].getAttribute('src');
									//if the image is on the current site
									if(imgsrc.indexOf(window.location.protocol+"//"+window.location.hostname)==0){
										var new_image = document.createElement('img');
										new_image.setAttribute('src',imgsrc);
										document.getElementById("precache_html_container").appendChild(new_image);
										;
									}
								}
								
							}
						}
						var timestamp = Math.round(new Date().getTime()/1000);						
						//store content
						if (typeof(Storage) !== "undefined") {
							if(!precache.existsInCookie(name)){
								try{
									localStorage.setItem(name,html_to_store);
									precache.addHTMLName(name);
									localStorage.setItem("timestamp__"+name,timestamp);
								}catch(e){
									try{
										sessionStorage.setItem(name,html_to_store);
										precache.addHTMLName(name);
										sessionStorage.setItem("timestamp__"+name,timestamp);										
									}catch(e){
										precache.limit_reached = true;
										console.log("No more space");
										
									}									
								}
							}
						} else {
							// Sorry! No Web Storage support..
						}				
					}
					else {
						console.log('something else other than 200 was returned');
					}
				}
			};
			xmlhttp.open("GET", url, true);
			xmlhttp.send();	
		}
	},

	
	
	//add the localStorage key in the cookie for the proxy
	addHTMLName: function(name){
		var pages_in_cookie = precache.readCookie("precache_pages");
		if(pages_in_cookie){
			//store the page reference in cookie
			var a_pages = pages_in_cookie.split('|');
			if(a_pages.indexOf(name)<0){
				a_pages.push(name);
				
			}
			pages_in_cookie = a_pages.join('|');
		}else{
			pages_in_cookie = name;
		}
		precache.createCookie("precache_pages",pages_in_cookie,false);
		
	},

	createCookie:function(name,value,days) {
		if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	},

	readCookie:function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},

	eraseCookie:function(name) {
		createCookie(name,"",-1);
	},
	
	existsInCookie:function(name) {
		var pages_in_cookie = precache.readCookie("precache_pages");
		if(pages_in_cookie){
			var a_pages = pages_in_cookie.split('|');
			return a_pages.indexOf(name)>=0;
		}	
		return false;
	},
	
	cleanOldEntries:function(duration){
		var current_timestamp = Math.round(new Date().getTime()/1000);		
		var pages_in_cookie = precache.readCookie("precache_pages");
		if(pages_in_cookie){
			var a_pages = pages_in_cookie.split('|');
			for (var i = 0;i<a_pages.length;i++) {				
				var name = a_pages[i];
				var page_timestamp = precache.getPageTimestamp(name);
				if(current_timestamp - page_timestamp > duration) {
					this.deleteHTML(name);
				}
			}
		}
		
	},
	
	getPageTimestamp:function(name){
		var timestamp = localStorage.getItem("timestamp__"+name);
		if(timestamp === null) {
			timestamp = sessionStorage.getItem("timestamp__"+name);
		}
		if(timestamp === null) {
			timestamp = 0;
		}
		return timestamp;
	},
	
	deleteHTML:function(name){
		var pages_in_cookie = precache.readCookie("precache_pages");
		if(pages_in_cookie){
			var a_pages = pages_in_cookie.split('|');
			var index = a_pages.indexOf(name);
			a_pages.splice(index,1);
			pages_in_cookie = a_pages.join('|');
		}else{
			pages_in_cookie = "";
		}	
		precache.createCookie("precache_pages",pages_in_cookie,false);
		window.localStorage.removeItem(name);
		window.sessionStorage.removeItem(name);
		window.localStorage.removeItem("timestamp__"+name);
		window.sessionStorage.removeItem("timestamp__"+name);
		console.log("deleted "+name);
	},
	
	//parse the current page for links and precache all pages
	parse_links_in_page:function() {
		var links = document.getElementsByTagName('a');
		for(var i in links){
			if(typeof links[i] == 'object'){
				var href = links[i].getAttribute('href');
				//if the link is on the current site
				if(href.indexOf(window.location.protocol+"//"+window.location.hostname)==0){
					//remove hashtag
					if(href.indexOf('#')>0){
						href = href.substr(0, href.indexOf('#'));
					}
					var path = href.replace(window.location.protocol+"//"+window.location.hostname,"");
					console.log(i+" "+path);
					//add to localStorage with the key equal to MD5 of URL without host
					this.addHTML(href, md5(path),false);
				}
			}
		}
	},
	
	unload_transition:function(event) {
		document.getElementById('unload_overlay').style.display="block";
	}
	
}

/*

*/

//MD5 function
function md5cycle(x, k) {
var a = x[0], b = x[1], c = x[2], d = x[3];

a = ff(a, b, c, d, k[0], 7, -680876936);
d = ff(d, a, b, c, k[1], 12, -389564586);
c = ff(c, d, a, b, k[2], 17,  606105819);
b = ff(b, c, d, a, k[3], 22, -1044525330);
a = ff(a, b, c, d, k[4], 7, -176418897);
d = ff(d, a, b, c, k[5], 12,  1200080426);
c = ff(c, d, a, b, k[6], 17, -1473231341);
b = ff(b, c, d, a, k[7], 22, -45705983);
a = ff(a, b, c, d, k[8], 7,  1770035416);
d = ff(d, a, b, c, k[9], 12, -1958414417);
c = ff(c, d, a, b, k[10], 17, -42063);
b = ff(b, c, d, a, k[11], 22, -1990404162);
a = ff(a, b, c, d, k[12], 7,  1804603682);
d = ff(d, a, b, c, k[13], 12, -40341101);
c = ff(c, d, a, b, k[14], 17, -1502002290);
b = ff(b, c, d, a, k[15], 22,  1236535329);

a = gg(a, b, c, d, k[1], 5, -165796510);
d = gg(d, a, b, c, k[6], 9, -1069501632);
c = gg(c, d, a, b, k[11], 14,  643717713);
b = gg(b, c, d, a, k[0], 20, -373897302);
a = gg(a, b, c, d, k[5], 5, -701558691);
d = gg(d, a, b, c, k[10], 9,  38016083);
c = gg(c, d, a, b, k[15], 14, -660478335);
b = gg(b, c, d, a, k[4], 20, -405537848);
a = gg(a, b, c, d, k[9], 5,  568446438);
d = gg(d, a, b, c, k[14], 9, -1019803690);
c = gg(c, d, a, b, k[3], 14, -187363961);
b = gg(b, c, d, a, k[8], 20,  1163531501);
a = gg(a, b, c, d, k[13], 5, -1444681467);
d = gg(d, a, b, c, k[2], 9, -51403784);
c = gg(c, d, a, b, k[7], 14,  1735328473);
b = gg(b, c, d, a, k[12], 20, -1926607734);

a = hh(a, b, c, d, k[5], 4, -378558);
d = hh(d, a, b, c, k[8], 11, -2022574463);
c = hh(c, d, a, b, k[11], 16,  1839030562);
b = hh(b, c, d, a, k[14], 23, -35309556);
a = hh(a, b, c, d, k[1], 4, -1530992060);
d = hh(d, a, b, c, k[4], 11,  1272893353);
c = hh(c, d, a, b, k[7], 16, -155497632);
b = hh(b, c, d, a, k[10], 23, -1094730640);
a = hh(a, b, c, d, k[13], 4,  681279174);
d = hh(d, a, b, c, k[0], 11, -358537222);
c = hh(c, d, a, b, k[3], 16, -722521979);
b = hh(b, c, d, a, k[6], 23,  76029189);
a = hh(a, b, c, d, k[9], 4, -640364487);
d = hh(d, a, b, c, k[12], 11, -421815835);
c = hh(c, d, a, b, k[15], 16,  530742520);
b = hh(b, c, d, a, k[2], 23, -995338651);

a = ii(a, b, c, d, k[0], 6, -198630844);
d = ii(d, a, b, c, k[7], 10,  1126891415);
c = ii(c, d, a, b, k[14], 15, -1416354905);
b = ii(b, c, d, a, k[5], 21, -57434055);
a = ii(a, b, c, d, k[12], 6,  1700485571);
d = ii(d, a, b, c, k[3], 10, -1894986606);
c = ii(c, d, a, b, k[10], 15, -1051523);
b = ii(b, c, d, a, k[1], 21, -2054922799);
a = ii(a, b, c, d, k[8], 6,  1873313359);
d = ii(d, a, b, c, k[15], 10, -30611744);
c = ii(c, d, a, b, k[6], 15, -1560198380);
b = ii(b, c, d, a, k[13], 21,  1309151649);
a = ii(a, b, c, d, k[4], 6, -145523070);
d = ii(d, a, b, c, k[11], 10, -1120210379);
c = ii(c, d, a, b, k[2], 15,  718787259);
b = ii(b, c, d, a, k[9], 21, -343485551);

x[0] = add32(a, x[0]);
x[1] = add32(b, x[1]);
x[2] = add32(c, x[2]);
x[3] = add32(d, x[3]);

}

function cmn(q, a, b, x, s, t) {
a = add32(add32(a, q), add32(x, t));
return add32((a << s) | (a >>> (32 - s)), b);
}

function ff(a, b, c, d, x, s, t) {
return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
return cmn(c ^ (b | (~d)), a, b, x, s, t);
}

function md51(s) {
txt = '';
var n = s.length,
state = [1732584193, -271733879, -1732584194, 271733878], i;
for (i=64; i<=s.length; i+=64) {
md5cycle(state, md5blk(s.substring(i-64, i)));
}
s = s.substring(i-64);
var tail = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
for (i=0; i<s.length; i++)
tail[i>>2] |= s.charCodeAt(i) << ((i%4) << 3);
tail[i>>2] |= 0x80 << ((i%4) << 3);
if (i > 55) {
md5cycle(state, tail);
for (i=0; i<16; i++) tail[i] = 0;
}
tail[14] = n*8;
md5cycle(state, tail);
return state;
}

/* there needs to be support for Unicode here,
 * unless we pretend that we can redefine the MD-5
 * algorithm for multi-byte characters (perhaps
 * by adding every four 16-bit characters and
 * shortening the sum to 32 bits). Otherwise
 * I suggest performing MD-5 as if every character
 * was two bytes--e.g., 0040 0025 = @%--but then
 * how will an ordinary MD-5 sum be matched?
 * There is no way to standardize text to something
 * like UTF-8 before transformation; speed cost is
 * utterly prohibitive. The JavaScript standard
 * itself needs to look at this: it should start
 * providing access to strings as preformed UTF-8
 * 8-bit unsigned value arrays.
 */
function md5blk(s) { /* I figured global was faster.   */
var md5blks = [], i; /* Andy King said do it this way. */
for (i=0; i<64; i+=4) {
md5blks[i>>2] = s.charCodeAt(i)
+ (s.charCodeAt(i+1) << 8)
+ (s.charCodeAt(i+2) << 16)
+ (s.charCodeAt(i+3) << 24);
}
return md5blks;
}

var hex_chr = '0123456789abcdef'.split('');

function rhex(n)
{
var s='', j=0;
for(; j<4; j++)
s += hex_chr[(n >> (j * 8 + 4)) & 0x0F]
+ hex_chr[(n >> (j * 8)) & 0x0F];
return s;
}

function hex(x) {
for (var i=0; i<x.length; i++)
x[i] = rhex(x[i]);
return x.join('');
}

function md5(s) {
return hex(md51(s));
}

/* this function is much faster,
so if possible we use it. Some IEs
are the only ones I know of that
need the idiotic second function,
generated by an if clause.  */

function add32(a, b) {
return (a + b) & 0xFFFFFFFF;
}

if (md5('hello') != '5d41402abc4b2a76b9719d911017c592') {
function add32(x, y) {
var lsw = (x & 0xFFFF) + (y & 0xFFFF),
msw = (x >> 16) + (y >> 16) + (lsw >> 16);
return (msw << 16) | (lsw & 0xFFFF);
}
}
//END MD5























window.addEventListener("load",precache.precache_start);
window.addEventListener("beforeunload",precache.unload_transition);



//AJAX interceptor
(function(open) {
    XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {

        this.addEventListener("readystatechange", function(evt) {
			
			if (this.readyState == 4) {
				if (this.responseText.substring(0,23)=="ajax-precache-reference") {

					var parts = this.responseText.split("=");
					var name = parts[1];
					Object.defineProperty(this, "responseText", {writable: true});			
					var content = localStorage.getItem(name);
					if(content===null){
						content = sessionStorage.getItem(name);
					}
					this.responseText = content;
				}
			}
        }, false);

        open.call(this, method, url, async, user, pass);
    };

})(XMLHttpRequest.prototype.open);

